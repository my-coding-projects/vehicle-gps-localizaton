# Documentation

## Task and solutions discussion:
* We receive a list of polygons from an external static source i.e. the number polygons will not change over time.
* And then, we have been also given a live end-point, which gives a list of cars in th ares of Stuttgart. 
* Each car has uniqueId (Vin) and current gps co-ordinates.

The task is to compute essentially two list:
1. List of polygons with cars.
2. List of cars with polygon(s).      

### Approach I : Grouping - The implemented solution.
* Since the polygons are constant at any given point of time, we could profile the polygons - as it will be one
time job (typically when we start the service).
* The idea is to find the "center" and "radius" of encompassing circle for every polygon.
* Check for circle intersection for each polygon pair - thereby we group the polygons into a 
list of "connected-polygons".
* In the worst case complexity - to group the polygons would be : n(n-1)/2 
* Optimization used is : When we are comparing the polygons, we group them dynamically. Thereby if a polygon
is found intersecting with the first polygon in a group then we need not to check with remaining polygons in the group. 
In worst case, we may find the connected polygon at the end of the list.
* And we finally, sort the "connected-polygons" list in the "ascending" order of their _size_.
* Now we have "polygon-connected-groups" in the ascending order already in hand.

#### How to figure out car belongs to a polygon? 
* Check if the car is in the circumference of every "connected-group" i.e every polygon in the group.
* The order of the  cars is - order in which, we get from the provided end-point.
* If the car is circumference, only then we go and check if it is _really inside_ the polygon. Checking if the 
car is inside the polygon requires O(p) computations, p is the number of points of the polygon.
* Also, we check for all the polygons in the group(if group has > 1). Therefore one car being in 
more than one polygon is not ruled out. Since, it is not said "explicitly", we assume this possibility 
could exist.
        
## Advantage of  "Approach I":
* If car is with in the circumference of first "connected-group" then we need not go further - even if it is not within
the polygon. 
* Therefore, we need not check in other groups.
    
## Drawbacks in "Approach I":
* In the worst case or if car is out of all the polygons - we end up in comparing with all the 
polygons - this is not very smart.
* Therefore,  _Approach II_ is proposed (below section).

## Improvements in Approach I (These are NOT implemented in the given code):
* If I had found out the _center_ and the _radius_ for the big polygon(formed by points of all the polygons) 
then I could have found out in one computation - if the car is outside vicinity of the given polygons. 
* If I had found out the center and the radius for every "connected-group"(which are computed) then I 
could have eliminated the whole group - if car is not in the vicinity of the _connected-group_. 
If distance b/w center of _connected-group_ to the car > Radius of _connected-group_ ==> Car is not in any polygon on the group.
   
   
### Approach II : Profiling the polygons w.r.t angle they occupy at a center point - This is NOT implemented.  
* In the above approach we only know the connected polygons. 
But, we do not get any idea about "how the polygons are distributed" and "span of individual polygon".

## Angle profiling of the polygons: 
* Find the center of for each polygon (if given not required).
 * Find the center (C) with all the polygons. This can be achieved by considering all the points in polygons 
as a single polygon.
* Take an arbitrary point from any one of the polygons as a reference point (Rp) - and don't change it.
* With _C_, _Rp_ and a _constant angle of rotation (clock or anti-clock)_, we can find out the _max_ 
and _min_ angle made by the polygon at C. The difference of _max_ and _min_ is the 
arc angle made by polygon.
* Therefore, now we are in a position to build a data structure to hold polygons for 
the given angle intervals. 
* Example: {[angle (0-30) - List1 (polygons)], [angle(31-60) - List2 (polygons)],...}. There could be
polygons which are in both List1 and List2.
* As special care should be taken for the polygons which are intersected by line connecting _C_ and
_Rp_. These polygons _max-min_ could result in negative angles (with some logic, this can also be taken care).

### Need for radius profiling:
* When and if we are dealing with large number of polygons and large areal distribution then we 
can consider of once again profiling 'List1' and 'List2' according to distance(radius) they are form the _C_.
* So 'List1' ==> {[radius(0-10) : List_a(polygons)], [radius(11-20) : List_b(polygons)],....} and so on.     

## Process of computing polygon(s) for a car:
* Find the distance between car and _C_. If the car is out side the circumference of _C_ - then car is NOT present
in any one of the given polygons. If it is inside - we go to next steps.
* Find out the angle between (C, Rp) and distance between car and C.
* With the angle in handle, use the above data structure to get the polygons, which are in overlapping this angle.
* Once we have the list of polygons - check if car is with in the circumference of the polygon - only if it is
with in the circumference - go for computing if car is within the polygon or not.

## Advantages:
* If the car is far way from all the polygons then we know it in one computation. In contract to first approach we have to compute all the polygons.
* Number of polygons to be compared will be proportional to the actual polygons distribution.
* With fine tuning the _angle profiling_ and _radius profiling_ - we can arrive at a balance trade-off point with max efficiency.     
* Flexibility for adopting to polygons distribution - like we can decide of 'radius profiling'.

## Challenges of angle (and radius) profiling:
* Relatively requires more computations for profiling(s) at start up. As this would be one time task, 
which will not effect the run time performance (computation time).
* May be, it requires more complex data structures than first approach. 

## Technical section
The above discussed  task is implemented as a spring-boot micro-service. Tools and technologies are presented below:
* Framework : Spring boot 2.1.4.RELEASE
* Programming Language : Java 8
* Build tool : Maven 3.5
* Java code generation from Json : [http://www.jsonschema2pojo.org/](http://www.jsonschema2pojo.org/)
* For some Geo calculation code is used from : [Project Geogeometry](https://github.com/jillesvangurp/geogeometry) 
* API documentation : Swagger 2
* Logging : Slf4j
* Help library to remove boiler plate code : Lombok 1.18
* For containerization : Docker

* To refresh the car incoming data, @Scheduled is used from spring-boot. To achieve synchronous behaviour
locks functionality is used, namely ReentrantLock is used. 
* I am not very sure whether 'ReentrantLock' is ideal for this use case or there exists some others, which 
suits better. 

 
#### Comments regarding dockerization:
* To achieve the communication between the _given_ and _developed_ micro-service, both are 
placed in a same docker network. 
* It looks, there are some advanced ways of addressing this problem for production deployments.
  
#### Create network:
    *   docker network create mynet

#### Run commands:
    *   docker run -d  --network mynet -p 3000:3000 --name api car2godeveloper/api-for-coding-challenge
    *   docker run --network mynet  -p 9999:9999 --name challenge  cartogo-challenge-service


### Further comments and improvements:
* Validations and null checks.
* Logs can be further refined and improved for better debugging purposes.
* Health end-points can be added.
* Security layer can be added.    
                