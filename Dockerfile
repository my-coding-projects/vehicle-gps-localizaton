FROM openjdk:8
ADD target/cartogo-challenge-service.jar cartogo-challenge-service.jar
EXPOSE 9999
ENTRYPOINT ["java", "-jar", "cartogo-challenge-service.jar"]