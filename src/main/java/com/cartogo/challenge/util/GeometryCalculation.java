package com.cartogo.challenge.util;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static java.lang.Math.*;

@Slf4j
public class GeometryCalculation {

    /**
     * Earth's mean radius, in meters.
     *
     * see http://en.wikipedia.org/wiki/Earth%27s_radius#Mean_radii
     */
    private static final double EARTH_RADIUS = 6371000.0;


    /**
     * Simple/naive method for calculating the center of a polygon based on
     * averaging the latitude and longitude. Better algorithms exist but this
     * may be good enough for most purposes.
     * Note, for some polygons, this may actually be located outside the
     * polygon.
     *
     * @param polygonPoints
     *            polygonPoints points that make up the polygon as arrays of
     *            [longitude,latitude]
     * @return the average longitude and latitude an array.
     */
    public static double[] polygonCenter(double[]... polygonPoints) {
        double cumLon = 0;
        double cumLat = 0;
        for (double[] coordinate : polygonPoints) {
            cumLon += coordinate[0];
            cumLat += coordinate[1];
        }
        return new double[] { cumLon / polygonPoints.length, cumLat / polygonPoints.length };
    }

    /**
     * Compute the Haversine distance between the two coordinates. Haversine is
     * one of several distance calculation algorithms that exist. It is not very
     * precise in the sense that it assumes the earth is a perfect sphere, which
     * it is not. This means precision drops over larger distances. According to
     * http://en.wikipedia.org/wiki/Haversine_formula there is a 0.5% error
     * margin given the 1% difference in curvature between the equator and the
     * poles.
     *
     * @param lat1
     *            the latitude in decimal degrees
     * @param long1
     *            the longitude in decimal degrees
     * @param lat2
     *            the latitude in decimal degrees
     * @param long2
     *            the longitude in decimal degrees
     * @return the distance in meters
     */
    public static double distance(final double lat1, final double long1, final double lat2, final double long2) {
        validate(lat1, long1, false);
        validate(lat2, long2, false);

        final double deltaLat = toRadians(lat2 - lat1);
        final double deltaLon = toRadians(long2 - long1);

        final double a = sin(deltaLat / 2) * sin(deltaLat / 2) + cos(Math.toRadians(lat1)) * cos(Math.toRadians(lat2)) * sin(deltaLon / 2) * sin(deltaLon / 2);

        final double c = 2 * asin(Math.sqrt(a));

        return EARTH_RADIUS * c;
    }

    /**
     * Find the point with is far away from the center and the
     * distance between them is taken as the radius.
     *
     */
    public static double radius(List<double[]> points, double[] center ){
        double temp_radius;
        double radius = 0;
        double[] farAwayPoint = null;
        for (double[] point : points) {
            temp_radius = GeometryCalculation.distance(center[0], center[1], point[0], point[1]);
            if (radius < temp_radius){
                radius = temp_radius;
                farAwayPoint = point;
            }
        }
        log.debug("The far away point is {}", farAwayPoint);
        return radius;
    }


    /**
     * Validates coordinates. Note. because of some edge cases at the extremes that I've encountered in several data sources, I've built in
     * a small tolerance for small rounding errors that allows e.g. 180.00000000000023 to validate.
     * @param latitude latitude between -90.0 and 90.0
     * @param longitude longitude between -180.0 and 180.0
     * @param strict if false, it will allow for small rounding errors. If true, it will not.
     * @throws IllegalArgumentException if the lat or lon is out of the allowed range.
     */
    private static void validate(double latitude, double longitude, boolean strict) {
        double roundedLat = latitude;
        double roundedLon = longitude;
        if(!strict) {
            // this gets rid of rounding errors e.g. 180.00000000000023 will validate
            roundedLat = Math.round(latitude*1000000)/1000000.0;
            roundedLon = Math.round(longitude*1000000)/1000000.0;
        }
        if(roundedLat < -90.0 || roundedLat > 90.0) {
            throw new IllegalArgumentException("Latitude " + latitude + " is outside legal range of -90,90");
        }
        if(roundedLon < -180.0 || roundedLon > 180.0) {
            throw new IllegalArgumentException("Longitude " + longitude + " is outside legal range of -180,180");
        }
    }

    public static boolean coordinate_is_inside_polygon_array(double latitude, double longitude,List<double[]> polygonPoints){
        double angle=0;
        double point1_lat;
        double point1_long;
        double point2_lat;
        double point2_long;
        int n = polygonPoints.size();
        for (int i=0;i<polygonPoints.size();i++) {
            point1_lat = polygonPoints.get(i)[0] - latitude;
            point1_long = polygonPoints.get(i)[1] - longitude;
            int whatIsThis = (i+1)%n;
            point2_lat = polygonPoints.get(whatIsThis)[0] - latitude;
            //you should have paid more attention in high school geometry.
            point2_long = polygonPoints.get(whatIsThis)[1] - longitude;
            angle += Angle2D(point1_lat,point1_long,point2_lat,point2_long);
        }
        return !(Math.abs(angle) < PI);

    }


    private static double Angle2D(double y1, double x1, double y2, double x2){
        double dtheta,theta1,theta2;
        theta1 = Math.atan2(y1,x1);
        theta2 = Math.atan2(y2,x2);
        dtheta = theta2 - theta1;
        while (dtheta > PI) dtheta -= 2 *PI;
        while (dtheta < -PI) dtheta += 2 *PI;
        return(dtheta);
    }

}
