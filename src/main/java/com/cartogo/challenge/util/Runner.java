/*
package com.cartogo.challenge.util;

import java.util.ArrayList;
import java.util.List;

public class Runner
{
    public static double PI = 3.14159265;
    public static double TWOPI = 2*PI;



    private static ArrayList<String> fetchHamburgPolygon(){
        ArrayList<String> polygon_lat_long_pairs = new ArrayList<String>();
        polygon_lat_long_pairs.add("53.847860, 8.658567");
        polygon_lat_long_pairs.add("53.154145, 8.962844");
        polygon_lat_long_pairs.add("52.860656, 9.879394");
        polygon_lat_long_pairs.add("53.029102, 11.005753");
        polygon_lat_long_pairs.add("53.592044, 11.249403");
        polygon_lat_long_pairs.add("54.267133, 10.801142");

        return polygon_lat_long_pairs;
    }

    private static List<double[]> fetchHamburgPolygonList(){
        List<double[]> polygon_lat_long_pairs = new ArrayList<>();
        polygon_lat_long_pairs.add(new double[]{53.847860, 8.658567});
        polygon_lat_long_pairs.add(new double[]{53.154145, 8.962844});
        polygon_lat_long_pairs.add(new double[]{52.860656, 9.879394});
        polygon_lat_long_pairs.add(new double[]{53.029102, 11.005753});
        polygon_lat_long_pairs.add(new double[]{53.592044, 11.249403});
        polygon_lat_long_pairs.add(new double[]{54.267133, 10.801142});

        return polygon_lat_long_pairs;
    }

    private static List<double[]> fetchNurembergPolygonList(){
        ArrayList<double[]> polygon_lat_long_pairs = new ArrayList<>();
        polygon_lat_long_pairs.add(new double[]{49.033556, 10.815415});
        polygon_lat_long_pairs.add(new double[]{49.128378, 11.610790});
        polygon_lat_long_pairs.add(new double[]{49.477276, 12.079416});
        polygon_lat_long_pairs.add(new double[]{49.866696, 11.288794});
        polygon_lat_long_pairs.add(new double[]{49.842243, 10.241119});
        polygon_lat_long_pairs.add(new double[]{49.524476, 9.958906});

        return polygon_lat_long_pairs;
    }

    private static ArrayList<String> fetchNurembergPolygon(){
        ArrayList<String> polygon_lat_long_pairs = new ArrayList<String>();

        polygon_lat_long_pairs.add("49.033556, 10.815415");
        polygon_lat_long_pairs.add("49.128378, 11.610790");
        polygon_lat_long_pairs.add("49.477276, 12.079416");
        polygon_lat_long_pairs.add("49.866696, 11.288794");
        polygon_lat_long_pairs.add("49.842243, 10.241119");
        polygon_lat_long_pairs.add("49.524476, 9.958906");

        return polygon_lat_long_pairs;
    }

    public static void main(String[] args) {
        ArrayList<Double> lat_array = new ArrayList<Double>();
        ArrayList<Double> long_array = new ArrayList<Double>();

        //This is the polygon bounding box, if you plot it,
        //you'll notice it is a rough tracing of the parameter of
        //the state of Florida starting at the upper left, moving
        //clockwise, and finishing at the upper left corner of florida.

        ArrayList<String> polygon_lat_long_pairs = fetchHamburgPolygon();

        //Convert the strings to doubles.
        for(String s : polygon_lat_long_pairs){
            lat_array.add(Double.parseDouble(s.split(",")[0]));
            long_array.add(Double.parseDouble(s.split(",")[1]));
        }

        //prints TRUE true because the lat/long passed in is
        //inside the bounding box.
        System.out.println(coordinate_is_inside_polygon(53.852157,8.700898, lat_array, long_array));

        System.out.println(coordinate_is_inside_polygon_array(53.852157,8.700898, fetchHamburgPolygonList()));

        //prints FALSE because the lat/long passed in
        //is Not inside the bounding box.
        System.out.println(coordinate_is_inside_polygon(48.380870,12.303174,lat_array, long_array));
        System.out.println(coordinate_is_inside_polygon_array(48.380870,12.303174, fetchHamburgPolygonList()));

    }

    public static boolean coordinate_is_inside_polygon_array(
            double latitude, double longitude, List<double[]> polygonPoints){
        double angle=0;
        double point1_lat;
        double point1_long;
        double point2_lat;
        double point2_long;
        int n = polygonPoints.size();
        System.out.println("n is "+n);
        for (int i=0;i<polygonPoints.size();i++) {
            point1_lat = polygonPoints.get(i)[0] - latitude;
            point1_long = polygonPoints.get(i)[1] - longitude;
            int whatIsThis = (i+1)%n;
            //System.out.println("(i+1)%n is = "+ whatIsThis);
            point2_lat = polygonPoints.get(whatIsThis)[0] - latitude;
            //you should have paid more attention in high school geometry.
            point2_long = polygonPoints.get(whatIsThis)[1] - longitude;
            angle += Angle2D(point1_lat,point1_long,point2_lat,point2_long);
        }
        return !(Math.abs(angle) < PI);

    }




    public static boolean coordinate_is_inside_polygon(
            double latitude, double longitude,
            ArrayList<Double> lat_array, ArrayList<Double> long_array)
    {
        int i;
        double angle=0;
        double point1_lat;
        double point1_long;
        double point2_lat;
        double point2_long;
        int n = lat_array.size();
        System.out.println("n is "+n);
        for (i=0;i<n;i++) {
            point1_lat = lat_array.get(i) - latitude;
            point1_long = long_array.get(i) - longitude;
            int whatIsThis = (i+1)%n;
            //System.out.println("(i+1)%n is = "+ whatIsThis);
            point2_lat = lat_array.get(whatIsThis) - latitude;
            //you should have paid more attention in high school geometry.
            point2_long = long_array.get(whatIsThis) - longitude;
            angle += Angle2D(point1_lat,point1_long,point2_lat,point2_long);
        }

        if (Math.abs(angle) < PI)
            return false;
        else
            return true;
    }

    public static double Angle2D(double y1, double x1, double y2, double x2)
    {
        double dtheta,theta1,theta2;

        theta1 = Math.atan2(y1,x1);
        theta2 = Math.atan2(y2,x2);
        dtheta = theta2 - theta1;
        while (dtheta > PI)
            dtheta -= TWOPI;
        while (dtheta < -PI)
            dtheta += TWOPI;

        return(dtheta);
    }

    public static boolean is_valid_gps_coordinate(double latitude,
                                                  double longitude)
    {
        //This is a bonus function, it's unused, to reject invalid lat/longs.
        if (latitude > -90 && latitude < 90 &&
                longitude > -180 && longitude < 180)
        {
            return true;
        }
        return false;
    }
}
*/