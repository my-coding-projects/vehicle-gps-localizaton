package com.cartogo.challenge.scheduler;

import com.cartogo.challenge.model.bean.Polygons;
import com.cartogo.challenge.model.bean.Vehicles;
import com.cartogo.challenge.model.dto.ChallengeBean;
import com.cartogo.challenge.model.generated.JsonVehicle;
import com.cartogo.challenge.model.pojo.Vehicle;
import com.cartogo.challenge.service.ComputationService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@Configuration
@Service
@Data
public class FetchCarsService {

    @Value("${external.url.fetchcars}")
    private String url_fetch_cars;

    private Polygons polygons;

    private ObjectMapper objectMapper;

    @Autowired
    public FetchCarsService(Polygons polygons,
                            ComputationService computationService,
                            ObjectMapper objectMapper){
        this.polygons = polygons;
        this.computationService = computationService;
        this.objectMapper = objectMapper;
    }

    private Vehicles vehicles;

    private List<JsonVehicle> jsonVehicles;

    private ComputationService computationService;

    private ChallengeBean challengeBean;

    private final ReentrantLock lock = new ReentrantLock();

    private boolean isComputing = false;

    private final Condition conditionComputationFinished = lock.newCondition();

    public ChallengeBean getChallengeBean(){
        while (isComputing) {
            try {
                conditionComputationFinished.await();
            } catch (InterruptedException e) {
                log.error("InterruptedException error ",e);
            }
        }
        return challengeBean;
    }


    /**
     * Change fixedDelay as required
     */
    @Scheduled(fixedDelay =   30 * 60 * 1000, initialDelay = 1000)
    public void fetch_vehicles(){
        log.info("Scheduler started at = {}", LocalTime.now());
        try {
            isComputing = true;
            lock.lock();
            fetchCarData();
            challengeBean = computationService.refresh(vehicles);
            isComputing = false;
            conditionComputationFinished.signal();
            log.info("Scheduler ended at = {}", LocalTime.now());
        } catch (Exception e) {
            log.error("Error while fetching the vehicles ", e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Fetch the car data from an external source
     */
    private void fetchCarData() {
        log.debug("Inside fetchCarData with url = {}", url_fetch_cars);
        try {
            vehicles = new Vehicles();
            jsonVehicles = objectMapper.readValue(new URL(url_fetch_cars), new TypeReference<List<JsonVehicle>>(){});
            log.info("Number of vehicles fetched = {}", jsonVehicles.size());
            extractVehicleInfo(jsonVehicles);
        } catch (IOException e) {
            log.error("Unexpected error occurred ", e);
        }
    }

    /**
     * Extract only the needed data from json info
     */
    private void extractVehicleInfo(List<JsonVehicle> jsonVehicles) {
        Vehicle vehicle;
        for (JsonVehicle j_vehicle: jsonVehicles) {
            vehicle = new Vehicle();
            vehicle.setVin(j_vehicle.getVin());
            vehicle.getGpsCoordinates()[0] = j_vehicle.getPosition().getLatitude();
            vehicle.getGpsCoordinates()[1] = j_vehicle.getPosition().getLongitude();
            vehicles.getVehicles().add(vehicle);
        }
    }
}
