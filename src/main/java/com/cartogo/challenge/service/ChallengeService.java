package com.cartogo.challenge.service;

import com.cartogo.challenge.exception.objects.PolygonNotFoundException;
import com.cartogo.challenge.exception.objects.VinNotFoundException;
import com.cartogo.challenge.model.dto.PolygonDTO;
import com.cartogo.challenge.model.dto.VehicleDTO;
import com.cartogo.challenge.scheduler.FetchCarsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ChallengeService {

    private FetchCarsService fetchCarsService;

    @Autowired
    public ChallengeService(FetchCarsService computeService){
        this.fetchCarsService= computeService;
    }

    /**
     *
     * @return all the vehicles and their respective polygons they belong to
     */
    public List<VehicleDTO> getVehicles(){
        return fetchCarsService.getChallengeBean().getVehicles();
    }

    /**
     *
     * @param vin search parameter
     * @return the respective car with and polygons it belongs to(if any)
     */
    public VehicleDTO getVehicle(String vin){
        if (fetchCarsService.getChallengeBean().getVin_polygons().containsKey(vin))
            return new VehicleDTO(vin, fetchCarsService.getChallengeBean().getVin_polygons().get(vin));
        throw new VinNotFoundException(vin);
    }

    /**
     *
     * @return all the polygons with cars in it
     */
    public List<PolygonDTO> getPolygons(){
        return fetchCarsService.getChallengeBean().getPolygons();
    }

    /**
     *
     * @param polygonId to search
     * @return the polygon and the cars, which are in it (at this point of time)
     */
    public PolygonDTO getPolygon(String polygonId) {
        if (fetchCarsService.getChallengeBean().getPolygon_vins().containsKey(polygonId)){
            return new PolygonDTO(polygonId, fetchCarsService.getChallengeBean().getPolygon_vins().get(polygonId));
        }
        throw new PolygonNotFoundException(polygonId);
    }
}
