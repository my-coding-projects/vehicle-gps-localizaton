package com.cartogo.challenge.service;

import com.cartogo.challenge.model.bean.Polygons;
import com.cartogo.challenge.model.bean.Vehicles;
import com.cartogo.challenge.model.dto.ChallengeBean;
import com.cartogo.challenge.model.dto.PolygonDTO;
import com.cartogo.challenge.model.dto.VehicleDTO;
import com.cartogo.challenge.model.pojo.ConnectedPolygons;
import com.cartogo.challenge.model.pojo.Polygon;
import com.cartogo.challenge.model.pojo.Vehicle;
import com.cartogo.challenge.util.GeometryCalculation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
@Service
public class ComputationService {

    private Polygons polygons;

    private Vehicles vehicles;

    private ChallengeBean bean;

    @Autowired
    public ComputationService(Polygons polygons){
        this.polygons = polygons;
    }

    public ChallengeBean refresh(Vehicles vehicles) {
        this.vehicles = vehicles;
        bean = new ChallengeBean();
        compute();
        bean.setPolygons(construct());
        return bean;
    }

    /**
     * It uses the injected polygons and vehicles to evaluate VINs in polygons
     * and polygons in VINs
     */
    private void compute(){
        List<String> polygons;
        for (Vehicle vehicle: vehicles.getVehicles()) {
            log.debug("Searching for VIN = {} ", vehicle.getVin());
            polygons = getPolygons(vehicle);
            bean.getVin_polygons().put(vehicle.getVin(), polygons);
            bean.getVehicles().add(new VehicleDTO(vehicle.getVin(), polygons));
        }
    }

    /**
     *For the vehicle:
     * Need to check for only the groups
     * If we find a group in which the vehicle is part of then we
     * need not go look in "other groups"
     */
    private List<String> getPolygons(Vehicle vehicle) {
        List<String> polygonIds = null;
        for (ConnectedPolygons connectedPolygons : polygons.getConnectedPolygons()) {
            polygonIds = checkVehicleInPolygonGroup(vehicle, connectedPolygons);
            if (polygonIds != null){
                break;
            }
        }
        return polygonIds == null ? new ArrayList<>() : polygonIds;
    }

    private List<PolygonDTO> construct() {
        List<PolygonDTO> polygonDTOList = new ArrayList<>();
        for (ConnectedPolygons connectedPolygons : polygons.getConnectedPolygons()) {
            for (Polygon polygon : connectedPolygons.getPolygons()){
                if (bean.getPolygon_vins().containsKey(polygon.getId()))
                    polygonDTOList.add(new PolygonDTO(polygon.getId(), bean.getPolygon_vins().get(polygon.getId())));
                else
                    polygonDTOList.add(new PolygonDTO(polygon.getId()));
            }
        }
        return polygonDTOList;
    }

    private List<String> checkVehicleInPolygonGroup(Vehicle vehicle, ConnectedPolygons connectedPolygons) {
        List<String> polygonIds = null;
        for (Polygon polygon : connectedPolygons.getPolygons()) {
            if (isVehicleInEncompassingCircleOfPolygon(vehicle, polygon)){
                polygonIds = new ArrayList<>();
                if (isVehicleInsidePolygon(vehicle, polygon)){
                    log.debug("It is in the polygon {}", polygon.getId());
                    polygonIds.add(polygon.getId());
                    addVinToPolygon(vehicle.getVin(), polygon.getId());
                }
            }
        }
        return polygonIds;
    }

    private void addVinToPolygon(String vin, String polygonId) {
        List<String> vinList;
        if (bean.getPolygon_vins().containsKey(polygonId)){
            bean.getPolygon_vins().get(polygonId).add(vin);
        }else {
            vinList = new ArrayList<>();
            vinList.add(vin);
            bean.getPolygon_vins().put(polygonId, vinList);
        }
    }

    private boolean isVehicleInsidePolygon(Vehicle vehicle, Polygon polygon) {
        return GeometryCalculation.coordinate_is_inside_polygon_array(
                vehicle.getGpsCoordinates()[0],
                vehicle.getGpsCoordinates()[1],
                polygon.getPoints());
    }

    private boolean isVehicleInEncompassingCircleOfPolygon(Vehicle vehicle, Polygon polygon) {
        double distBetVehicleAndCenterOfPolygon = GeometryCalculation.distance(
                vehicle.getGpsCoordinates()[0],
                vehicle.getGpsCoordinates()[1],
                polygon.getCenter()[0],
                polygon.getCenter()[1]);
        return distBetVehicleAndCenterOfPolygon <= polygon.getRadius();
    }
}
