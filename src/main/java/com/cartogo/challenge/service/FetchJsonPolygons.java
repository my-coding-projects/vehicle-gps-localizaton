package com.cartogo.challenge.service;

import com.cartogo.challenge.model.bean.JPolygons;
import com.cartogo.challenge.model.generated.JsonPolygon;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.List;

@Service
@Slf4j
public class FetchJsonPolygons {

    @Value("${external.url}")
    private String external_url;

    private ObjectMapper objectMapper;

    public FetchJsonPolygons(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    @Bean
    public JPolygons jPolygons(){
        List<JsonPolygon> jsonPolygonsList = null;
        JPolygons jPolygons;
        jPolygons = new JPolygons();
        try {
            jsonPolygonsList = objectMapper.readValue(new URL(external_url), new TypeReference<List<JsonPolygon>>(){});
        }catch (Exception e){
            log.error("Error occurred while fetching polygons from external source ", e);
        }
        jPolygons.setJsonPolygons(jsonPolygonsList);
        return jPolygons;
    }
}
