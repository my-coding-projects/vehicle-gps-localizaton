package com.cartogo.challenge.controller;

import com.cartogo.challenge.model.dto.PolygonDTO;
import com.cartogo.challenge.model.dto.VehicleDTO;
import com.cartogo.challenge.service.ChallengeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@Api(value="CarToGo Coding  Challenge")

public class ChallengeController {

    private ChallengeService carsInPolygons;

    @Autowired
    public ChallengeController(ChallengeService challengeService){
        this.carsInPolygons = challengeService;
    }

    @GetMapping(value = "/data/vins" ,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all vins",responseContainer = "List", response = VehicleDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved vins ist")
    })
    public ResponseEntity<List<VehicleDTO>> getVins(){
        return ResponseEntity.ok(carsInPolygons.getVehicles());
    }

    @GetMapping(value = "/data/vins/{vin}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Search a vin", response = VehicleDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved vin details")})
    public ResponseEntity<VehicleDTO> fetchAllVinData(@PathVariable String vin){
        log.debug("Searching for vin = {}", vin);
        return ResponseEntity.ok(carsInPolygons.getVehicle(vin));
    }

    @GetMapping(value = "/data/polygons" ,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all polygons", responseContainer = "List", response = PolygonDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved polygons list")
    })
    public ResponseEntity<List<PolygonDTO>> fetchAllPolygonData(){
        return ResponseEntity.ok(carsInPolygons.getPolygons());
    }

    @GetMapping(value = "/data/polygons/{polygon}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Search a polygon", response = PolygonDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved polygon details")
    })
    public ResponseEntity<PolygonDTO> fetchPolygon(@PathVariable String polygon){
        log.debug("Searching for the polygon = {} ", polygon);
        return ResponseEntity.ok(carsInPolygons.getPolygon(polygon));
    }

}
