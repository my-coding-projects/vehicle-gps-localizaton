package com.cartogo.challenge.exception.objects;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unknown Vin")
public class VinNotFoundException extends RuntimeException{
    public VinNotFoundException(String unknown_vin){
        log.error("Vin = {} is unknown", unknown_vin);
    }
}
