package com.cartogo.challenge.exception.objects;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unknown Polygon Id")
public class PolygonNotFoundException extends RuntimeException{
    public PolygonNotFoundException(String unknown_polygon){
        log.error("Polygon Id = {} is unknown", unknown_polygon);
    }
}
