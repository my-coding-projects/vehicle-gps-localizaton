package com.cartogo.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class PolygonDTO {

    public PolygonDTO(String polygonId){
        this.polygonId = polygonId;
        vin_list = new ArrayList<>();
    }

    @JsonProperty("polygon_id")
    private String polygonId;

    @JsonProperty("vin_list")
    private List<String> vin_list;

}
