package com.cartogo.challenge.model.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@ApiModel(description = "Full details")
public class ChallengeBean {

    private Map<String, List<String>> vin_polygons = new HashMap<>();

    private List<VehicleDTO> vehicles = new ArrayList<>();

    private Map<String, List<String>> polygon_vins = new HashMap<>();

    private List<PolygonDTO> polygons = new ArrayList<>();

}
