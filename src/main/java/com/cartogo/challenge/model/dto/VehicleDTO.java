package com.cartogo.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class VehicleDTO {

    @JsonProperty("vin")
    private String vin;

    @JsonProperty("polygon_list")
    private List<String> polygon_list;
}
