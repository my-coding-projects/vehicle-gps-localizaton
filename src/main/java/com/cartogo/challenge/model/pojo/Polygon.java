package com.cartogo.challenge.model.pojo;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class Polygon {

    private String id;

    private List<double[]> points = new ArrayList<>();

    private double[] center;

    private double radius;

    private List<String> vins = new ArrayList<>();
}
