
package com.cartogo.challenge.model.generated;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "active",
    "is_excluded",
    "area"
})
public class Options {

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("is_excluded")
    private Boolean isExcluded;
    @JsonProperty("area")
    private Double area;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
