
package com.cartogo.challenge.model.generated;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "min",
    "max",
    "idle_time",
    "revenue",
    "walking_range1",
    "walking_range2"
})
public class ActiveTimedOptions {

    @JsonProperty("min")
    private Integer min;
    @JsonProperty("max")
    private Integer max;
    @JsonProperty("idle_time")
    private Integer idleTime;
    @JsonProperty("revenue")
    private Integer revenue;
    @JsonProperty("walking_range1")
    private Integer walkingRange1;
    @JsonProperty("walking_range2")
    private Integer walkingRange2;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
