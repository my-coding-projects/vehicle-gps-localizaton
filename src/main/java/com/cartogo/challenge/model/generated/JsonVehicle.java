
package com.cartogo.challenge.model.generated;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "locationId",
    "vin",
    "numberPlate",
    "position",
    "fuel",
    "model"
})
public class JsonVehicle {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("locationId")
    private Integer locationId;
    @JsonProperty("vin")
    private String vin;
    @JsonProperty("numberPlate")
    private String numberPlate;
    @JsonProperty("position")
    private Position position;
    @JsonProperty("fuel")
    private Double fuel;
    @JsonProperty("model")
    private String model;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("locationId")
    public Integer getLocationId() {
        return locationId;
    }

    @JsonProperty("locationId")
    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @JsonProperty("vin")
    public String getVin() {
        return vin;
    }

    @JsonProperty("vin")
    public void setVin(String vin) {
        this.vin = vin;
    }

    @JsonProperty("numberPlate")
    public String getNumberPlate() {
        return numberPlate;
    }

    @JsonProperty("numberPlate")
    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    @JsonProperty("fuel")
    public Double getFuel() {
        return fuel;
    }

    @JsonProperty("fuel")
    public void setFuel(Double fuel) {
        this.fuel = fuel;
    }

    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
