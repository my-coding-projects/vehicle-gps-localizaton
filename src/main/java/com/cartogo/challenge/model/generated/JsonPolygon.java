
package com.cartogo.challenge.model.generated;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_id",
    "updatedAt",
    "createdAt",
    "__v",
    "name",
    "cityId",
    "legacyId",
    "type",
    "geoFeatures",
    "options",
    "timedOptions",
    "geometry",
    "version",
    "$computed"
})
public class JsonPolygon {

    @JsonProperty("_id")
    private String id;
    @JsonProperty("updatedAt")
    private String updatedAt;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("__v")
    private Integer v;
    @JsonProperty("name")
    private String name;
    @JsonProperty("cityId")
    private String cityId;
    @JsonProperty("legacyId")
    private String legacyId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("geoFeatures")
    private List<GeoFeature> geoFeatures = null;
    @JsonProperty("options")
    private Options options;
    @JsonProperty("timedOptions")
    private List<TimedOption> timedOptions = null;
    @JsonProperty("geometry")
    private Geometry_ geometry;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("$computed")
    private com.cartogo.challenge.model.generated.$computed $computed;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
