package com.cartogo.challenge.model.bean;

import com.cartogo.challenge.model.generated.JsonPolygon;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JPolygons {

    private List<JsonPolygon> jsonPolygons = new ArrayList<>();
}
