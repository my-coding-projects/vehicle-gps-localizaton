package com.cartogo.challenge.model.bean;

import com.cartogo.challenge.model.pojo.ConnectedPolygons;
import lombok.Data;

import java.util.List;

@Data
public class Polygons {
    private List<ConnectedPolygons> connectedPolygons;
}
