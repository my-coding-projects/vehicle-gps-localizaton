package com.cartogo.challenge.model.bean;

import com.cartogo.challenge.model.pojo.Vehicle;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Vehicles {

    private List<Vehicle> vehicles = new ArrayList<>();
}
