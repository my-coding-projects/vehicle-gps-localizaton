package com.cartogo.challenge.base;

import com.cartogo.challenge.model.bean.JPolygons;
import com.cartogo.challenge.model.bean.Polygons;
import com.cartogo.challenge.model.generated.JsonPolygon;
import com.cartogo.challenge.model.pojo.ConnectedPolygons;
import com.cartogo.challenge.model.pojo.Polygon;
import com.cartogo.challenge.util.GeometryCalculation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Configuration
@Slf4j
public class ProfilePolygonsService {

    private JPolygons jPolygons;

    @Autowired
    public ProfilePolygonsService(JPolygons jPolygons){
        this.jPolygons= jPolygons;
    }

    @Bean
    public Polygons profile(){
        Polygons polygons = new Polygons();
        int initial_polygons_list;
        int cumulative_polygons_count = 0;
        List<ConnectedPolygons> connectedPolygonsList;
        List<JsonPolygon> jsonPolygonsList;
        try {
            jsonPolygonsList = jPolygons.getJsonPolygons();
            initial_polygons_list = jsonPolygonsList.size();
            log.debug("Loaded Polygons List {}", jsonPolygonsList);
            log.info("Number of Polygons loaded : {}", jsonPolygonsList.size());

            List<Polygon> polygonList = extract(jsonPolygonsList);
            log.debug("After trimming out extra data, size is {} ", polygonList.size());

            connectedPolygonsList = group(polygonList);
            log.info("Number of connected polygon are = {}", connectedPolygonsList.size());

            for (ConnectedPolygons connectedPolygons : connectedPolygonsList) {
                cumulative_polygons_count = cumulative_polygons_count + connectedPolygons.getPolygons().size();
            }
            log.info("The cumulative count of polygons of connected polygons are {}", cumulative_polygons_count);

            if (cumulative_polygons_count != initial_polygons_list){
                log.error("Grouping of polygons went wrong. Expected = {}, Actual = {} ",initial_polygons_list, cumulative_polygons_count );
                System.exit(0);
            }
            polygons.setConnectedPolygons(connectedPolygonsList);
        } catch (Exception e) {
            log.error("error while loading the polygon from external URL", e);
        }
        return polygons;
    }


    /**
     * From the bulk/external json data, extract only the required polygon details.
     *
     */
    private List<Polygon> extract(List<JsonPolygon> jpolygons){
        List<Polygon> polygons = new ArrayList<>();
        jpolygons.forEach(jsonPolygon -> {
            Polygon polygon = new Polygon();
            polygon.setId(jsonPolygon.getId());
            log.debug("Polygon ID = {} ", polygon.getId());
            jsonPolygon.getGeometry().getCoordinates().get(0).forEach(jpoint -> {
                double[] point = new double[2];
                point[0] = jpoint.get(1);
                point[1] = jpoint.get(0);
                log.debug(point[0]+","+point[1]);
                polygon.getPoints().add(point);
            });

            //Finding the center of encompassing circle of the polygon
            double[] center = GeometryCalculation.polygonCenter(polygon.getPoints().toArray(new double[0][1]));
            polygon.setCenter(center);
            log.debug("Center of polygon is = "+ Arrays.toString(center));

            //Get radius of the encompassing circle the polygon
            double radius = GeometryCalculation.radius(polygon.getPoints(), center);
            log.debug("Radius of encompassing circle is = {}", radius);
            polygon.setRadius(radius);
            polygons.add(polygon);
        });

        return polygons;
    }



    /**
     * Each polygon has center and radius of encompassing circle.
     * We find the distance between every two polygons and if the
     * distance < radius(P1) + radius(P2), then we say they are connected polygon.
     * This could be a polygon in a polygon or just overlapping.
     */
    private List<ConnectedPolygons> group(List<Polygon> polygons){
        List<ConnectedPolygons> connectedPolygons = new ArrayList<>();
        List<Polygon> intersecting_polygons;
        Polygon a_polygon_from_list;
        for (int i = 0; i < polygons.size(); i++) {
                intersecting_polygons = new ArrayList<>();
                intersecting_polygons.add(polygons.get(i));

            for (int j = i+1; j < polygons.size(); j++) {
                a_polygon_from_list = polygons.get(j);
                for (int k = 0; k < intersecting_polygons.size(); k++) {
                    Polygon intersect_polygon = intersecting_polygons.get(k);
                    double dist_bet_polygons = GeometryCalculation.distance(
                                                a_polygon_from_list.getCenter()[0],
                                                a_polygon_from_list.getCenter()[1],
                                                intersect_polygon.getCenter()[0],
                                                intersect_polygon.getCenter()[1]);
                    if (dist_bet_polygons <= a_polygon_from_list.getRadius() + intersect_polygon.getRadius()) {
                        intersecting_polygons.add(a_polygon_from_list);
                        polygons.remove(j);
                        j = j - 1;
                        break;
                    }
                }
            }

            if (intersecting_polygons.size()>1){
                log.debug("Intersecting Polygons Start");
                intersecting_polygons.forEach(polygon -> {
                    log.debug("##### Polygon ###");
                    polygon.getPoints().forEach(pt -> log.debug(pt[0]+","+pt[1]));
                });
                log.debug("Intersecting Polygons End");
            }
            connectedPolygons.add(new ConnectedPolygons(intersecting_polygons));
        }
        connectedPolygons.sort(Comparator.comparingInt(ConnectedPolygons::groupSize));
        return connectedPolygons;
    }
}
