package com.cartogo.challenge.service;

import com.cartogo.challenge.model.bean.Polygons;
import com.cartogo.challenge.model.bean.Vehicles;
import com.cartogo.challenge.model.dto.ChallengeBean;
import com.cartogo.challenge.model.pojo.ConnectedPolygons;
import com.cartogo.challenge.model.pojo.Polygon;
import com.cartogo.challenge.model.pojo.Vehicle;
import com.cartogo.challenge.util.GeometryCalculation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class ComputationServiceTest {


    @InjectMocks
    private ComputationService computationService;

    @Mock
    private Vehicles vehicles;

    @Mock
    private Polygons polygons;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void positive_test_car_in_polygon() {
        Mockito.when(polygons.getConnectedPolygons()).thenReturn(getHamburg());
        Mockito.when(vehicles.getVehicles()).thenReturn(carInsideHamburg());
        ChallengeBean bean = computationService.refresh(vehicles);
        Assert.assertEquals(carVinInsideHamburg(), bean.getPolygon_vins().get(hamburgPolygonId()).get(0));
        Assert.assertEquals(hamburgPolygonId(), bean.getVin_polygons().get(carVinInsideHamburg()).get(0) );
    }

    @Test
    public void negative_test_car_not_in_polygon() {
        Mockito.when(polygons.getConnectedPolygons()).thenReturn(getHamburg());
        Mockito.when(vehicles.getVehicles()).thenReturn(carOutsideHamburg());
        ChallengeBean bean = computationService.refresh(vehicles);
        Assert.assertNull( bean.getPolygon_vins().get(hamburgPolygonId()));
        Assert.assertEquals(0, bean.getVin_polygons().get(carVinOutsideHamburg()).size());
    }

    private List<ConnectedPolygons> getHamburg(){

        List<ConnectedPolygons> connectedPolygonsList = new ArrayList<>();
        List<Polygon> polygonList = new ArrayList<>();
        Polygon hamburg = new Polygon();

        hamburg.getPoints().add(new double[]{53.847860, 8.658567});
        hamburg.getPoints().add(new double[]{53.154145, 8.962844});
        hamburg.getPoints().add(new double[]{52.860656, 9.879394});
        hamburg.getPoints().add(new double[]{53.029102, 11.005753});
        hamburg.getPoints().add(new double[]{53.592044, 11.249403});
        hamburg.getPoints().add(new double[]{54.267133, 10.801142});

        hamburg.setId(hamburgPolygonId());

        hamburg.setCenter(GeometryCalculation.polygonCenter(hamburg.getPoints().toArray(new double[0][1])));
        hamburg.setRadius(GeometryCalculation.radius(hamburg.getPoints(), hamburg.getCenter()));

        polygonList.add(hamburg);
        connectedPolygonsList.add(new ConnectedPolygons(polygonList));

        return connectedPolygonsList;
    }

    private List<Vehicle> carInsideHamburg() {
        List<Vehicle> vehicleList = new ArrayList<>();
        vehicleList.add(new Vehicle(carVinInsideHamburg(), new double[]{53.852157, 8.700898}));
        return vehicleList;
    }

    private List<Vehicle> carOutsideHamburg() {
        List<Vehicle> vehicleList = new ArrayList<>();
        vehicleList.add(new Vehicle(carVinOutsideHamburg(), new double[]{48.8111614, 10.2891424}));
        return vehicleList;
    }

    private String hamburgPolygonId() {return "Hamburg Polygon Id";}

    private String carVinInsideHamburg(){return "VIN-Inside Hamburg";}

    private String carVinOutsideHamburg(){return "VIN-Outside Hamburg";}



}