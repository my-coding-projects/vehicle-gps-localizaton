package com.cartogo.challenge.base;

import com.cartogo.challenge.model.bean.JPolygons;
import com.cartogo.challenge.model.bean.Polygons;
import com.cartogo.challenge.model.generated.Geometry_;
import com.cartogo.challenge.model.generated.JsonPolygon;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProfilePolygonsServiceTest {

    @Mock
    private JPolygons jPolygons;

    @InjectMocks
    private ProfilePolygonsService profilePolygonsService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void positive_two_connected_polygons() {
        Mockito.when(jPolygons.getJsonPolygons()).thenReturn(getHamburgs_connected_polygons());
        Polygons polygons = profilePolygonsService.profile();
        Assert.assertEquals(1, polygons.getConnectedPolygons().size());
    }

    @Test
    public void negative_not_connected_polygons(){
        Mockito.when(jPolygons.getJsonPolygons()).thenReturn(getHamburg_Ulm_not_connected_polygons());
        Polygons polygons = profilePolygonsService.profile();
        Assert.assertEquals(2, polygons.getConnectedPolygons().size());
    }

    private List<JsonPolygon> getHamburgs_connected_polygons(){
        List<JsonPolygon> jsonPolygons = new ArrayList<>();
        jsonPolygons.add(hamburg_polygon_one());
        jsonPolygons.add(hamburg_polygon_two());
        return jsonPolygons;
    }

    private List<JsonPolygon> getHamburg_Ulm_not_connected_polygons(){
        List<JsonPolygon> jsonPolygons = new ArrayList<>();
        jsonPolygons.add(hamburg_polygon_one());
        jsonPolygons.add(ulm_polygon_one());
        return jsonPolygons;
    }

    private JsonPolygon hamburg_polygon_one(){
        JsonPolygon jsonPolygon = new JsonPolygon();
        jsonPolygon.setId("hamburg_polygon_one");
        jsonPolygon.setGeometry(hamburg_polygon_one_geometry());
        return jsonPolygon;
    }

    private JsonPolygon ulm_polygon_one(){
        JsonPolygon jsonPolygon = new JsonPolygon();
        jsonPolygon.setId("ulm_polygon_one");
        jsonPolygon.setGeometry(ulm_polygon_one_geometry());
        return jsonPolygon;
    }

    private JsonPolygon hamburg_polygon_two(){
        JsonPolygon jsonPolygon = new JsonPolygon();
        jsonPolygon.setId("hamburg_polygon_two");
        jsonPolygon.setGeometry(hamburg_polygon_two_geometry());
        return jsonPolygon;
    }


    private Geometry_ hamburg_polygon_one_geometry(){
        Geometry_ geometry_ = new Geometry_();
        List<List<List<Double>>> coordinates = new ArrayList<>();
        List<List<Double>> list_2 = new ArrayList<>();
        list_2.add(new ArrayList<>(Arrays.asList(53.847860, 8.658567)));
        list_2.add(new ArrayList<>(Arrays.asList(53.154145, 8.962844)));
        list_2.add(new ArrayList<>(Arrays.asList(52.860656, 9.879394)));
        list_2.add(new ArrayList<>(Arrays.asList(53.029102, 11.005753)));
        list_2.add(new ArrayList<>(Arrays.asList(53.592044, 11.249403)));
        list_2.add(new ArrayList<>(Arrays.asList(54.267133, 10.801142)));
        coordinates.add(list_2);
        geometry_.setCoordinates(coordinates);
        return geometry_;
    }

    private Geometry_ hamburg_polygon_two_geometry(){
        Geometry_ geometry_ = new Geometry_();
        List<List<List<Double>>> coordinates = new ArrayList<>();
        List<List<Double>> list_2 = new ArrayList<>();
        list_2.add(new ArrayList<>(Arrays.asList(53.55108, 9.99368)));
        list_2.add(new ArrayList<>(Arrays.asList(53.08625, 9.45369)));
        list_2.add(new ArrayList<>(Arrays.asList(52.74841, 10.6457)));
        list_2.add(new ArrayList<>(Arrays.asList(52.99377, 11.73335)));
        list_2.add(new ArrayList<>(Arrays.asList(53.50322, 12.11238)));
        list_2.add(new ArrayList<>(Arrays.asList(53.81085, 11.23897)));
        list_2.add(new ArrayList<>(Arrays.asList(53.76055, 10.33534)));
        coordinates.add(list_2);
        geometry_.setCoordinates(coordinates);
        return geometry_;
    }

    private Geometry_ ulm_polygon_one_geometry(){
        Geometry_ geometry_ = new Geometry_();
        List<List<List<Double>>> coordinates = new ArrayList<>();
        List<List<Double>> list_2 = new ArrayList<>();
        list_2.add(new ArrayList<>(Arrays.asList(48.42358, 9.37292)));
        list_2.add(new ArrayList<>(Arrays.asList(48.18974, 9.54321)));
        list_2.add(new ArrayList<>(Arrays.asList(48.17142, 10.18591)));
        list_2.add(new ArrayList<>(Arrays.asList(48.50735, 10.54846)));
        list_2.add(new ArrayList<>(Arrays.asList(48.68536, 9.98267)));
        list_2.add(new ArrayList<>(Arrays.asList(48.69262, 9.52124)));
        coordinates.add(list_2);
        geometry_.setCoordinates(coordinates);
        return geometry_;
    }
}